import os
import json
import asyncio
import logging
import threading
import tornado.web
import tornado.ioloop

from crafty.helpers import Console
from crafty.helpers import Helpers

console = Console()
helpers = Helpers()

logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

with open(logging_path, 'r') as logging_file:
    logging_config = json.load(logging_file)
logging.config.dictConfig(logging_config)
logger = logging.getLogger()


class MessageHandler(tornado.web.RequestHandler):
    web_root = os.path.join(os.path.curdir, 'crafty', 'web')

    def load_rules(self):
        rules_path = os.path.join(self.web_root, "rules.txt")
        rules = []

        with open(rules_path,"r") as rulesfile:
            for line in rulesfile:
                rules.append(line.rstrip())

        return rules


    def load_about(self):
        about_path = os.path.join(self.web_root, "about.txt")

        with open(about_path, 'r') as file:
            data = file.read().replace('\n', '')

        return data

    def load_status(self):
        status_path = os.path.join(self.web_root, "status.html")

        with open(status_path, 'r') as file:
            data = file.read()

        return data


    def get(self):

        about_str = self.load_about()
        rules_lst = self.load_rules()
        status_html = self.load_status()
        server_name_str = helpers.get_setting("http_server", 'server_name')

        self.render(
            "index.html",
            server_name=server_name_str,
            about=about_str,
            rules=rules_lst,
            server_status=status_html
        )



def run_tornado():
    port_number = helpers.get_setting('http_server', 'port_number')
    web_root = os.path.join(os.path.curdir, 'crafty', 'web')

    logging.info("Starting Tornado HTTP Server on port {}...".format(port_number))
    asyncio.set_event_loop(asyncio.new_event_loop())

    app = tornado.web.Application([(r'/', MessageHandler)], template_path=web_root)
    app.listen(port_number)
    tornado.ioloop.IOLoop.instance().start()

def start_web_server():
    thread = threading.Thread(target=run_tornado, daemon=True)
    thread.start()




