from colorama import init
from termcolor import colored

import datetime
import os
import json
import logging
import sys

logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

with open(logging_path, 'r') as logging_file:
    logging_config = json.load(logging_file)
logging.config.dictConfig(logging_config)


# makes a pretty console
class Console(object):

    currentDT = datetime.datetime.now().strftime("%Y-%m-%d %I:%M:%S %p")

    def __int__(self):
        init()

    def debug(self, message):
        print(colored("[+] Crafty: {} - DEBUG:\t{}".format(self.currentDT, message), 'white'))

    def info(self, message):
        print(colored("[+] Crafty: {} - INFO:\t{}".format(self.currentDT, message), 'white'))

    def warning(self, message):
        print(colored("[+] Crafty: {} - WARNING:\t{}".format(self.currentDT, message), 'cyan'))

    def error(self, message):
        print(colored("[+] Crafty: {} - ERROR:\t{}".format(self.currentDT, message), 'yellow'))

    def critical(self, message):
        print(colored("[+] Crafty: {} - CRITICAL:\t{}".format(self.currentDT, message), 'red'))

    def help(self, message):
        print(colored("[+] Crafty: {} - HELP:\t{}".format(self.currentDT, message), 'green'))


#helpers to keep code cleaner
class Helpers:

    console = Console()

    def ensure_dir_exists(self, path):
        try:
            os.makedirs(path)
        # directory already exists - non-blocking error
        except FileExistsError:
            pass

    # checks to see if a file exists
    def check_file_exists(self, path):
        logging.debug('Looking for path: {}'.format(path))

        if os.path.exists(path) and os.path.isfile(path):
            logging.debug('Found path: {}'.format(path))
            return True
        else:
            logging.warning('Unable to find path: {}'.format(path))
            return False

    # creates a blank settings file if one isn't there
    def create_default_settings(self):
        global settings_path
        default_settings = {
            "crafty": {
                "branch": "beta"
            },
            "server": {
                "path": "/path/to/server",
                "jar": "paperclip.jar",
                "memory_max": 1024,
                "memory_min": 1024,
                "additional_args": ""
            },
            "updates": {
                "update_backup_folder": "pre_update_backups",
                "jar_type": "paper",
                "paper_url": "https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/paperclip.jar",
                "bukkit_url": "https://getbukkit.org/get/CiNKyh4l9MuPHLpovnGSDU2oHT9gCpUc",
                "spigot_url": "https://getbukkit.org/get/68aef01121494a41fe71890b81d69d07",
                "other_url": "https://pointme/to/your.jar"
            },
            "logging": {
                "log_file_level": "info"
            },
            "backups": {
              "backup_dir": "/path/to/server/crafty_backups"
            }
        }

        with open(settings_path, 'w') as fp:
            try:
                json.dump(default_settings, fp)
            except Exception as e:
                logging.critical("Unable to make default settings file: {}".format(e))
                self.console.critical("Unable to make default settings file - Check the log file for more info")

    # returns a setting from the settings.ini file - also does some error checking
    def get_setting(self, section, setting, get_all=None):

        global console

        settings_path = os.path.join(os.getcwd(), 'crafty', 'settings.json')

        with open(settings_path) as json_data_file:
            config = json.load(json_data_file)

        if get_all is not None:
            return json.dumps(config, indent=4)

        else:
            section = section.lower()
            setting = setting.lower()

            try:
                return config[section][setting]

            except Exception as exception:
                logging.critical("Unable to find, or make a section:{} setting:{}".format(section, setting))
                self.console.critical("Unable to find, or make a section:{} setting:{}".format(section, setting))
                sys.exit(1)

    def zippath(self, path, zipfile_handle):
        for root, dirs, files in os.walk(path):
            for file in files:
                zipfile_handle.write(os.path.join(root, file))