import os
import re
import sys
import json
import time
import shlex
import psutil
import zipfile
import datetime
import subprocess
import logging.config

from .helpers import Console
from .helpers import Helpers

logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

with open(logging_path, 'r') as logging_file:
    logging_config = json.load(logging_file)
logging.config.dictConfig(logging_config)
logger = logging.getLogger()


# our server class
class Server:

    logger = logging.getLogger()
    console = Console()
    helpers = Helpers()

    def __init__(self, server_command=None, server_path=None):
        # holders for our process
        self.process = None
        self.line = False
        self.PID = None
        self.start_time = None
        self.server_jar = None

        if server_command is not None:
            logging.debug('Server Command set to {}'.format(server_command))
            self.server_command = server_command

        if server_path is not None:
            logging.debug('Server CWD set to {}'.format(server_path))
            self.server_path = server_path

    def run(self):
        try:
            logging.info("Launching Server with command {}".format(self.server_command))
            self.process = subprocess.Popen(shlex.split(self.server_command),
                                            stdin=subprocess.PIPE,
                                            stdout=subprocess.PIPE,
                                            cwd=self.server_path,
                                            shell=False,
                                            universal_newlines=True)

        except Exception as err:
            logging.critical("Unable to start server!")
            self.console.critical("Unable to start server!")
            sys.exit(0)

        self.PID = self.process.pid
        ts = time.time()
        self.start_time = str(datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'))
        logging.info("Server Running with PID: {}".format(self.PID))

    def send_command(self, command):
        logging.debug('Sending Command: {} to Server via stdin'.format(command))

        # encode the command
        command.encode()

        # send it
        self.process.stdin.write(command + '\n')

        # flush the buffer to send the command
        self.process.stdin.flush()

        # give the command time to finish
        time.sleep(1)

        '''
        grab the results via the log - 
        
        Captains log star date: 10/08/2019 - Hours spent in vain
        
        Turns out python isn't so good about sending data to a process and getting a return without killing the 
        process. I've googled for hours on the subject and while communicate is a good avenue, it kills the process. 
        So this is the best solution I can think of, without adding more modules from Pypi - not that I saw any that 
        would for sure fix this process. I'll just Issue the command and then grab the results from the log since 
        everything is logged to the server log anyway. And 20 lines should be enough as the standard window isn't 
        that large anyway. '''
        return self.tail(None, 20)

    def stop_server(self):
        logging.info('Sending Stop Command: to Server')
        self.console.info('Stopping Server')
        self.send_command('Stop')

        for x in range(6):

            if self.check_running():
                logging.debug('Polling says server is running')
                self.console.info('Waiting 10 seconds for all threads to close. Stop command issued {} seconds ago'.format(x * 10))
                time.sleep(10)

            # now the server is dead, we set process to none
            else:
                logging.debug('Server Stopped')
                self.console.info("Server Stopped")
                self.process = None
                self.PID = None
                self.start_time = None
                # return true as the server is down
                return True

        # if we got this far, the server isn't responding, and needs to be forced down
        logging.critical('Unable to stop the server - asking the user if they want to force it down')
        self.console.critical('The server PID:{} isn\'t responding to stop commands!'.format(self.PID))

        resp = input("Do you want to force the server down? y/n >")
        logging.warning('User responded with {}'.format(resp.lower))

        # ask the parse the response
        if resp.lower() == "y":
            self.console.warning("Trying to kill the process")

            # try to kill it with fire!
            self.killpid(self.PID)

            # wait a few seconds to see if we can really kill it
            time.sleep(5)

            # let them know the outcome
            if self.check_running():
                self.console.critical("Unable to kill the process - It's still running")
            else:
                self.console.info("Process was killed successfully")
        else:
            self.console.critical("No worries - I am letting the server run")
            self.console.critical("The stop command was still sent though, it might close later, or is unresponsive.")

    def restart_server(self):
        logging.info('Restarting the server')

        resp = input("Do you want to warn users? y/n >")
        logging.warning('User responded with {} when asked to warn users'.format(resp.lower))

        if resp.lower() == "y":
            self.console.warning("Letting the users know they got 60 seconds")
            self.send_command('Say - Craft Controller is restarting the server in 60 seconds!')
            time.sleep(60)
        else:
            self.send_command('Say - Craft Controller is restarting the server!')

        self.console.warning("Shutting down the server for restart")

        # stop the server
        self.stop_server()

        self.console.warning("Waiting 10 seconds for everything to spin down")

        # give everything time to shutdown
        time.sleep(10)

        logging.info("Starting Server after Restart")
        self.console.info("Starting Server after Restart")

        self.run()

    def check_running(self):
        # if process is None, we never tried to start
        if self.process is None:
            return False

        else:
            # poll to see if it's still running - None = Running | Negative Int means Stopped
            poll = self.process.poll()

            if poll is None:
                return True
            else:
                # reset process to None for next run
                self.process = None
                return False

    def get_pid(self):
        return self.PID

    def get_start_time(self):
        if self.check_running():
            return self.start_time
        else:
            return False

    # tails any file, by lines, defaults to latest.log
    def tail(self, file=None, n=1, bs=1024):

        if file is None:
            file = os.path.join(self.server_path, 'logs', 'latest.log')

        if not self.helpers.check_file_exists(file):
            return False

        # read the log file
        f = open(file)
        f.seek(0, 2)
        l = 1 - f.read(1).count('\n')
        B = f.tell()
        while n >= l and B > 0:
            block = min(bs, B)
            B -= block
            f.seek(B, 0)
            l += f.read(block).count('\n')
        f.seek(B, 0)
        l = min(l, n)
        lines = f.readlines()[-l:]
        f.close()

        # if we have lines...
        if len(lines) >= 1:
            self.console.info('Showing last {} lines of log data \n '.format(n))
            for line in lines:
                print(line, end='')
        else:
            self.console.warning("Unable to read logfile")

    def killpid(self, pid):
        logging.info('Killing Process {} and all child processes'.format(pid))
        process = psutil.Process(pid)

        # for every sub process...
        for proc in process.children(recursive=True):
            # kill all the child processes - it sounds too wrong saying kill all the children
            logging.info('Killing process {}'.format(proc.name))
            proc.kill()
        # kill the main process we are after
        logging.info('Killing parent process')
        process.kill()

    def get_world_name(self):
        search_string = 'level-name*'
        worldname = self.search_server_properties(search_string)
        if worldname:
            return worldname
        else:
            return False

    # because this is a recursive function, we will return bytes, and set human readable later
    def get_dir_size(self, path):
        total = 0
        for entry in os.scandir(path):
            if entry.is_dir(follow_symlinks=False):
                total += self.get_dir_size(entry.path)
            else:
                total += entry.stat(follow_symlinks=False).st_size
        return total

    def human_readable_sizes(self,num, suffix='B'):
        for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
            if abs(num) < 1024.0:
                return "%3.1f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f %s%s" % (num, 'Yi', suffix)

    # returns the first setting that = the regex supplied
    def search_server_properties(self, regex='*'):

        # whats the file we are looking for?
        server_prop_file = os.path.join(self.server_path, 'server.properties')

        # re of what we are looking for
        # ignoring case - just in case someone used all caps
        pattern = re.compile(regex, re.IGNORECASE)

        # make sure it exists
        if self.helpers.check_file_exists(server_prop_file):
            with open(server_prop_file, 'rt') as f:
                for line in f:
                    # if we find something
                    if pattern.search(line) is not None:
                        match_line = line.rstrip('\n').split("=", 2)

                        # if we have at least 2 items in the list (i.e. there was an = char
                        if len(match_line) == 2:
                            return match_line[1]

            # if we got here, we couldn't find it
            logging.warning('Unable to find string using regex {} in server.properties file'.format(regex))
            self.console.warning('Unable to find string using regex {} in server.properties file'.format(regex))
            return False

        # if we got here, we can't find server.properties (bigger issues)
        logging.warning('Unable to find server.properties file')
        self.console.warning('Unable to find server.properties file')
        return False

    # returns a list of list of matching lines in the file searched
    def search_file(self, file_to_search, word='info'):

        # list of lines we are returning
        return_lines = []

        logging.debug("Searching for {} in {} ".format(word, file_to_search))

        # make sure it exists
        if self.helpers.check_file_exists(file_to_search):

            # line number
            line_num = 0

            with open(file_to_search, 'rt') as f:

                for line in f:
                    line_num += 1

                    # if we find something
                    if re.search(word.lower(), line.lower()) is not None:

                        logging.debug("Found Line that matched: {}".format(line))
                        match_line = line.rstrip('\n')

                        # add this match to the list of lines
                        return_lines.append([line_num, match_line])

        else:
            # if we got here, we couldn't find it
            logging.debug('Unable to find string {} in {}'.format(word, file_to_search))

        return return_lines

    # returns a dictionary of device information (device/total/used/free)
    def get_disk_stats(self):
        # list of our devices
        devices = []

        for part in psutil.disk_partitions(all=False):
            usage = psutil.disk_usage(part.mountpoint)

            if 'loop' not in part.device:
                devices.append({
                    'device': part.device,
                    'total': self.human_readable_sizes(usage.total),
                    'used': self.human_readable_sizes(usage.used),
                    'free': self.human_readable_sizes(usage.free),
                })

        if len(devices) >= 1:
            return devices
        else:
            return False

    # shows the ops on the server
    def get_ops_file(self):
        # ops file path
        ops_path = os.path.join(self.server_path, 'ops.json')

        # if it exists...
        if self.helpers.check_file_exists(ops_path):
            with open(ops_path) as json_data_file:
                ops = json.load(json_data_file)
                return json.dumps(ops, indent=4)
        else:
            logging.error('Unable to get Ops.json file')
            self.console.error('Unable to get Ops.json file')

    def backup_worlds(self, announce=True):

        # get the backup dir from the settings file
        backup_path = self.helpers.get_setting('backups', 'backup_dir')

        world_name = self.get_world_name()

        logging.info('Starting Backup Process')

        if announce:
            self.console.info("Starting Backup of Worlds")

        if backup_path:

            full_backup_file_path = None

            # if server is running
            if self.check_running():

                # let everyone know the server is backing up
                logging.debug('Server up - Informing everyone we are backing up')
                self.send_command("say [Crafty Controller] Doing World Backup")

            try:
                logging.debug('Checking Backup Path Exists')

                # make sure we have a place to put backups
                self.helpers.ensure_dir_exists(backup_path)

                # make sure we have a backup for this date
                backup_sub_dir = os.path.join(backup_path,datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'))

                # make sure this backup sub directory exists
                self.helpers.ensure_dir_exists(backup_sub_dir)

                # build our crazy dictionary of worlds, pathnames, and such
                full_world_backup_paths_dict = [
                        {"full_world_path": os.path.join(self.server_path, world_name),
                            "backup_filename": os.path.join(
                                backup_sub_dir,
                                world_name + "_" +
                                datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + ".zip")
                        },
                        {"full_world_path": os.path.join(self.server_path, world_name + "_nether"),
                            "backup_filename": os.path.join(
                                backup_sub_dir,
                                world_name + "_nether_" +
                                datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + ".zip")
                        },
                        {"full_world_path": os.path.join(self.server_path, world_name + "_the_end"),
                            "backup_filename": os.path.join(
                                backup_sub_dir,
                                world_name + "the_end_" +
                                datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + ".zip")
                        },
                    ]

                for world in full_world_backup_paths_dict:
                    logging.debug("Starting Backup Archive of {} - in dir {}".format(world['full_world_path'],
                                                                                     world['backup_filename']))
                    zip_handler = zipfile.ZipFile(world['backup_filename'], 'w')
                    self.helpers.zippath(world['full_world_path'], zip_handler)
                    zip_handler.close()
                    logging.debug('Created Backup Archive of {} - in dir {}'.format(world['full_world_path'],
                                                                                     world['backup_filename']))

                    logging.info("Backup Completed")

                    if announce:
                        self.console.info("Completed Backup of Worlds - World: {}".format(world['full_world_path']))
            except:
                logging.error('Unable to create backups')
                if announce:
                    self.console.error('Unable to create backups - Hopefully the logs will help with this issue')

        else:
            logging.error("Unable to find backup path in settings file!")
            self.console.error("Unable to find backup path in settings file!")
            return False

    def search_for_errors(self):
        log_file = os.path.join(self.server_path, "logs", "latest.log")

        errors = self.search_file(log_file, "ERROR]")
        warnings = self.search_file(log_file, "WARN]")
        exceptions = self.search_file(log_file, "exception")

        # create a joined list
        joinedlist = errors + warnings + exceptions

        # sort the list by line number
        if len(joinedlist) > 1:
            sorted(joinedlist, key=lambda x: x[0])

        return joinedlist

    def check_orphanged_server(self):

        # loop through processes
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if 'java' in proc.name().lower():

                    # join the command line together so we can search it for the server.jar
                    cmdline = " ".join(proc.cmdline())

                    server_jar = self.helpers.get_setting('server', 'jar')

                    if server_jar is None:
                        return False

                    # if we found the server jar in the command line, and the process is java, we can assume it's an
                    # orphaned server.jar running
                    if server_jar in cmdline:

                        # set p as the process / hook it
                        p = psutil.Process(proc.pid)
                        pidcreated = datetime.datetime.fromtimestamp(p.create_time())

                        logging.info("Another server found! PID:{}, NAME:{}, CMD:{} ".format(
                            p.pid,
                            p.name(),
                            cmdline
                        ))

                        self.console.warning("We found another process running the server.jar.")
                        self.console.warning("Process ID: {}".format(p.pid))
                        self.console.warning("Process Name: {}".format(p.name()))
                        self.console.warning("Process Command Line: {}".format(cmdline))
                        self.console.warning("Process Started: {}".format(pidcreated))

                        resp = input("Do you wish to kill this other server process? y/n > ")

                        if resp.lower() == 'y':
                            self.console.warning('Attempting to kill process: {}'.format(p.pid))

                            # kill the process
                            p.terminate()
                            # give the process time to die
                            time.sleep(2)
                            self.console.warning('Killed: {}'.format(proc.pid))
                            self.console.info("Running check again to be sure nothing else is running the server.jar")
                            self.check_orphanged_server()

            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass

        return False



