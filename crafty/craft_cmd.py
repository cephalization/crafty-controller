import os
import re
import cmd
import sys
import json
import requests
import logging

from crafty.helpers import Console
from crafty.helpers import Helpers
from crafty._version import __version__

console = Console()
helpers = Helpers()

logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

with open(logging_path, 'r') as logging_file:
    logging_config = json.load(logging_file)
logging.config.dictConfig(logging_config)
logger = logging.getLogger()

class CraftyCmd(cmd.Cmd):

    """ Crafty Prompt - Used to control the Crafty Module"""

    prompt = "Crafty Subsystem:> "

    intro = "Loaded Crafty Subsystem: \n"
    intro += "The Crafty Subsystem is a simple CLI processor to help control the Crafty System \n"
    intro += "Type Exit to get back to the main system \n"
    ruler = '-'

    def default(self, line):
        console.warning('Unknown Command')


    def do_EOF(self, line):
        """ Ctrl-D shortcut used to exit the whole system"""
        try:
            os.system('clear')
        except:
            os.system('cls')
        finally:
            sys.exit(0)

    def can_exit(self):
        return True

    def do_exit(self, s):
        return True

    def help_exit(self):
        console.help("Exit Craft Prompt, Going back to the main program.")
        console.help("You can also use Ctrl-D to close everything")

    def do_get_public_ip(self, line):
        r = requests.get('http://ipinfo.io/ip')
        if r.text:
            console.info('Your Public IP is: {}'.format(r.text))
        else:
            console.warning("Unable to find your public IP!")

    def help_get_public_ip(self):
        console.help("Gets your public IP address")

    def do_show_config(self, line):
        console.info("Here is your current config: \n {}".format(helpers.get_setting(None, None, True)))

    def help_show_config(self):
        console.help("Shows your current settings for Crafty stored in crafty/settings.json")

    def do_clear(self, line):
        try:
            os.system('clear')
        except:
            os.system('cls')

    def help_clear(self):
        console.help("Clears the screen")

    def do_show_version(self, line):
        console.info("You are running Crafty Controller v: {}".format(__version__))

    def help_show_version(self):
        console.help("Returns the current Crafty Controller Version - Show Updates will also check for updates")

    def do_check_update(self, line):
        logging.info("Checking for updates")

        # show current version to remind them
        self.do_show_version(line)

        # get the branch
        branch = helpers.get_setting("crafty", 'branch')
        console.info("You are running branch: {}".format(branch.title()))

        pattern = re.compile('"(.*?)"', re.IGNORECASE)

        r = requests.get('https://gitlab.com/Ptarrant1/crafty-controller/raw/{}/crafty/_version.py'.format(branch))
        if r.text:

            logging.debug("Update check returned: {}".format(r.text))

            if pattern.search(r.text) != None:
                match = pattern.search(r.text)
                version = match.group(1)
                console.info('The Latest Version of Crafty Controller is: v {}'.format(version))

            # if we have something weird with the version (like lack of quotes) let's just show it
            else:
                console.info("Latest Version is {}".format(r.text))
        else:
            console.warning("Unable to find updates!")

        console.info("For the Latest information Visit: https://gitlab.com/ptarrant1/crafty-controller/")

    def help_check_update(self):
        console.help("Shows the current version, the branch you are running, and the newest version according to gitlab")