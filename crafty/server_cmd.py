import os
import sys
import cmd
import json
import time
import psutil
import shutil
import logging
import requests
import datetime
import threading
import platform
import schedule

from crafty.server import Server

from crafty.helpers import Console
from crafty.helpers import Helpers

import crafty.http

console = Console()
helpers = Helpers()

logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

with open(logging_path, 'r') as logging_file:
    logging_config = json.load(logging_file)
logging.config.dictConfig(logging_config)
logger = logging.getLogger()


class ServerCmd(cmd.Cmd):
    """ Server Prompt - Used to control the Server Module"""

    prompt = "Server Subsystem:> "

    intro = "Loaded Server Subsystem: \n"
    intro += "The Server Subsystem is a simple CLI processor to help control your Minecraft Server \n"
    intro += "Type Exit to get back to the main system \n"

    ruler = '-'

    def __int__(self):
        self.server_thread = None
        self.svr = None
        self.scheduler = None
        self.http_server = None

    # this is fire off before the main loop of the class, this way svr var is already setup before commands are taken.
    def preloop(self):
        self.setup_svr_obj()

        logging.info("Starting Scheduler Daemon")
        self.scheduler = threading.Thread(name='Scheduler', target=self.start_scheduler, daemon=True)
        self.scheduler.start()

    # sets up the svr instance for the class - called from preloop
    def setup_svr_obj(self):
        logging.debug("Server Module Loaded")

        server_config = self.get_server_run_command()
        self.svr = Server(server_config['run_command'], server_config['server_path'])

        logging.info("Checking for orphaned server")
        self.svr.check_orphanged_server()

    # returns server command - and does some basic error checking
    def get_server_run_command(self):

        return_data = {}

        # configure the server from settings.ini
        server_path = helpers.get_setting('server', 'path')
        server_jar = helpers.get_setting('server', 'jar')
        server_max_mem = helpers.get_setting('server', 'memory_max')
        server_min_mem = helpers.get_setting('server', 'memory_min')
        server_args = helpers.get_setting('server', 'additional_args')

        # set this to something for now
        server_exec_path = os.path.join(server_path, server_jar)

        if not helpers.check_file_exists(server_exec_path):
            logging.critical("Server path set to: {}".format(server_exec_path))
            logging.critical("Unable to find server as defined in the settings file! - trying default location")

            console.critical("Server path set to: {}".format(server_exec_path))
            console.critical("Unable to find server as defined in the settings file! - trying default location")

            default_server_jar_path = os.path.join(os.getcwd(), 'server', 'paperclip.jar')

            if not helpers.check_file_exists(default_server_jar_path):
                logging.critical("Unable to find server in default location!")
                logging.critical("Server path set to: {}".format(default_server_jar_path))

                console.critical("Server path set to: {}".format(default_server_jar_path))
                console.critical("Unable to find server in default location!")
                console.critical("Please edit the settings.json file here: {}!"
                                 .format(os.path.join(os.getcwd(), "crafty", "settings.json"))
                                 )
                sys.exit(1)

            # server is in default location
            else:
                server_exec_path = default_server_jar_path

        return_data['run_command'] = "java -Xms{} -Xmx{} -jar {} nogui {}".format(server_min_mem, server_max_mem,
                                                                                  server_exec_path, server_args)
        return_data['server_path'] = server_path

        return return_data

    def start_scheduler(self):
        self.read_schedules()

        while True:
            logging.debug("Scheduler Running any pending jobs")
            schedule.run_pending()
            # wait 30 seconds, we don't need to be too exact since we are only doing minutes/hours
            time.sleep(30)

    def read_schedules(self):
        logging.debug('Reading scheduled tasks and scheduling')

        # read the current settings
        backup = helpers.get_setting('scheduler', 'backups')
        status = helpers.get_setting('scheduler', 'show_status')
        scheduled_command = helpers.get_setting('scheduler', 'scheduled_command')
        web_server = helpers.get_setting('http_server', 'enabled')

        if backup['enabled']:
            logging.info('Scheduled backup for every {} {}'.format(backup['interval'], backup['interval_type']))

            if backup['interval_type'] == 'minutes':
                schedule.every(backup['interval']).minutes.do(self.svr.backup_worlds, False)
            else:
                schedule.every(backup['interval']).hours.do(self.svr.backup_worlds, False)

        if status['enabled']:
            logging.info('Scheduled show_status for every {} {}'.format(status['interval'], status['interval_type']))
            if status['interval_type'] == 'minutes':
                schedule.every(status['interval']).minutes.do(self.do_show_status, '')
            else:
                schedule.every(status['interval']).hours.do(self.do_show_status, '')


        if scheduled_command['enabled']:
            logging.info('Scheduled Command: {} running every {} {}'.format(scheduled_command['command'],
                                                                                scheduled_command['interval'],
                                                                                scheduled_command['interval_type']))
            if scheduled_command['interval_type'] == 'minutes':
                schedule.every(scheduled_command['interval']).minutes.do(self.do_invoke_command,
                                                                         scheduled_command['command'])
            else:
                schedule.every(scheduled_command['interval']).hours.do(self.do_show_status,
                                                                       scheduled_command['command'])

        if web_server:
            # write status to a file in web
            self.write_html_server_status()

            # schedule it to run ever minute
            schedule.every(1).minute.do(self.write_html_server_status)

            # setup the webserver
            crafty.http.start_web_server()

    def write_html_server_status(self):
        logging.debug('Creating System Stats HTML')

        cpu_usage = psutil.cpu_percent(interval=0.5) / psutil.cpu_count()
        cpu_cores = psutil.cpu_count()
        load_avg = psutil.getloadavg()

        mem_percent = psutil.virtual_memory()[2]
        boot_time = datetime.datetime.fromtimestamp(psutil.boot_time())

        web_root = os.path.join(os.path.curdir, 'crafty', 'web')
        status_file = os.path.join(web_root, 'status.html')
        f = open(status_file, "w")

        if self.svr.check_running():
            f.write("Minecraft Server Started at:<br /> <b>{}</b>. <br /><br />".format(self.svr.get_start_time()))
        else:
            f.write("Minecraft Server is <b>not running </b>  <br /><br />")

        f.write("CPU Usage: <b>{}%</b> across {} Cores.  <br />".format(cpu_usage, cpu_cores))
        f.write("MEMORY Usage: <b>{}%</b>.  <br /><br />".format(mem_percent))
        f.write("My Average Load is <b>{}</b>.  <br /><br />".format(load_avg))

        f.close()

    def do_show_schedule(self, line):
        backup = helpers.get_setting('scheduler', 'backups')
        status = helpers.get_setting('scheduler', 'show_status')
        scheduled_command = helpers.get_setting('scheduler', 'scheduled_command')

        if backup['enabled']:
            console.info("Scheduled Backups are Enabled: every {} {}".format(backup['interval'], backup['interval_type']))
        else:
            console.info("Scheduled Backups are Disabled")

        if status['enabled']:
            console.info("Scheduled Status' are Enabled: every {} {}".format(status['interval'], status['interval_type']))
        else:
            console.info("Scheduled Status' are Disabled")

        if scheduled_command['enabled']:
            console.info('Scheduled Command: "{}" running every {} {}'.format(scheduled_command['command'],
                                                                                scheduled_command['interval'],
                                                                                scheduled_command['interval_type']))
        else:
            console.info("Scheduled Command' is Disabled")

    def help_show_schedule(self):
        console.help("This will show the scheduled commands you are running, and how often they run")

    def default(self, line):
        console.warning('Unknown Command')

    def do_EOF(self, line):
        """ Ctrl-D shortcut used to exit the whole system"""
        sys.exit(0)

    def can_exit(self):
        return True

    def do_exit(self, s):

        # if the server is still running, show a warning
        if self.svr.check_running():
            console.warning("The server is still running! You can still exit, and the server will stay running, however")
            console.warning("Crafty won't be able to hook back into the process and it will be orphaned.")

            # ask if they want to exit still
            resp = input("Do you still want to exit? y/n > ")
            if resp.lower() == 'y':
                # exit
                console.info("The server is still running. It's PID is: {}".format(self.svr.get_pid()))
                return True
            # abort the exit
            else:
                return False

        # if the server isn't running, we don't care if they exit.
        return True

    def help_exit(self):
        console.help("Exit Server Prompt, Going back to the main program.")
        console.help("You can also use Ctrl-D to close everything")

    # starts the minecraft subprocess as a thread (daemon)
    def do_start(self, line):
        if self.svr.check_running():
            console.warning("Server already running...")
        else:
            console.info("Starting server...")
            self.server_thread = threading.Thread(target=self.svr.run(), daemon=True)
            self.server_thread.start()

    def help_start(self):
        console.help("Starts the Minecraft Server if not already running")

    # stops the minecraft subprocess
    def do_stop(self, line):
        if self.svr.check_running():
            self.svr.stop_server()
            self.server_thread.join()
        else:
            console.error("Server isn't running - Try issuing a start command")

    def help_stop(self):
        console.help("Stop the Minecraft Server if running")

    # returns server status - system specs, and server running status
    def do_show_status(self, line):
        os, name, version, _, _, _ = platform.uname()
        version = version.split('-')[0]

        cpu_usage = psutil.cpu_percent(interval=0.5) / psutil.cpu_count()
        cpu_cores = psutil.cpu_count()
        load_avg = psutil.getloadavg()

        mem_percent = psutil.virtual_memory()[2]

        disk_percent = psutil.disk_usage('/')[3]

        boot_time = datetime.datetime.fromtimestamp(psutil.boot_time())
        running_time = boot_time.strftime('%Y-%m-%d %H:%M:%S')

        errors = self.svr.search_for_errors()

        console.info("I am currently running on {} version {}. ".format(os, version))
        console.info("This system is named {} and has {} CPU cores. ".format(name, cpu_cores))
        console.info("Current CPU Usage is {} percent. ".format(cpu_usage))
        console.info("Current MEMORY Usage is {} percent. ".format(mem_percent))
        console.info("Current DISK Usage is {} percent. ".format(disk_percent))
        console.info("I have been running since {}. ".format(running_time))
        console.info("My Average Load is {}. ".format(load_avg))

        if self.svr.check_running():
            console.info("Minecraft Server Started at {}. ".format(self.svr.get_start_time()))
        else:
            console.info("Minecraft Server is not running")

        if errors:
            console.info("{} errors or warnings were found in your latest server logs".format(len(errors)))
            console.help("run show_errors to see the errors or warnings in your server logs")
        else:
            console.info("No errors or warnings were found in your latest server logs")

    def help_show_status(self):
        console.help("Shows the server status. Minecraft JAR running / CPU / Memory / Disk Usage Included")

    def do_clear(self, line):
        try:
            os.system('clear')
        except:
            os.system('cls')

    def help_clear(self):
        console.help("Clears the screen")

    def do_show_log(self, line=20):

        if line == "":
            line = 20

        try:
            # make sure it's a number passed
            loglines = int(line)
            self.svr.tail(None, loglines)

        except ValueError:
            console.warning("{} is not a number - Please only use numbers for arguments to the log function")
            self.help_show_log()

    def help_show_log(self):
        console.help('Displays n lines of the latest.log from your server running.')
        console.help('Example: Log 20 will show the last 20 lines of the server log file')
        console.help('Suggestion: 100 lines should be the max here, but you do you')

    def do_invoke_command(self, line):
        if self.svr.check_running():
            log_output = self.svr.send_command(line)
            print(log_output)
        else:
            console.warning("Server isn't running - Try issuing a start command")

    def help_invoke_command(self):
        console.help('This is used to invoke any command on the server (without the /)')
        console.help('It will return the last 20 lines of the log for the response')

    def do_restart(self, line):
        self.svr.restart_server()

    def help_restart(self):
        console.help('This will restart the server. You will have the option to warn users before it restarts')
        console.help('To keep load low, and to make sure everything closes correctly, there are several pauses')
        console.help('built into the command. You could also run stop / start manually if you wish. )')
        console.help('This is just a shortcut')

    def do_show_world(self,line):
        logging.debug('Getting World info')

        world = self.svr.get_world_name()
        logging.info("Level detected as {}".format(world))

        disk_stats = None

        # if we have/found a world, look for the nether, end ones as well
        if world:
            console.info("Your level name is set to {}".format(world))
            level_path = os.path.join(self.svr.server_path, world)
            nether_path = os.path.join(self.svr.server_path, world + "_nether")
            end_path = os.path.join(self.svr.server_path, world + "_the_end")

            level_total_size = self.svr.get_dir_size(level_path)
            nether_total_size = self.svr.get_dir_size(nether_path)
            end_total_size = self.svr.get_dir_size(end_path)

            # doing a sep line to keep readable
            level_total_size = self.svr.human_readable_sizes(level_total_size)
            nether_total_size = self.svr.human_readable_sizes(nether_total_size)
            end_total_size = self.svr.human_readable_sizes(end_total_size)

            disk_stats = self.svr.get_disk_stats()

            if disk_stats is None:
                disk_stats = {
                    'device': 'Unable to get Disk Information',
                    'total': 'Unable to get Disk Information',
                    'used': 'Unable to get Disk Information',
                    'free': 'Unable to get Disk Information',
                }

            # logging
            logging.info("{} is {}".format(world, level_total_size))
            logging.info("{} is {}".format(world + "_nether", nether_total_size))
            logging.info("{} is {}".format(world + "_the_end", end_total_size))
            logging.info('Device Info as follows: {}'.format(disk_stats))

            # console printing
            console.info("{} is {}".format(world, level_total_size))
            console.info("{} is {}".format(world + "_nether", nether_total_size))
            console.info("{} is {}\n".format(world + "_the_end", end_total_size))

            console.info("Here is your Disk Information ")
            for disk in disk_stats:
                console.info("Device: {} \t {} Total \t {} Used \t {} Free ".format(disk['device'], disk['total'], disk['used'], disk['free']))
        else:
            logging.warning("Unable to detect your level names - Is the server.properties valid?")
            console.warning("Unable to detect your level names - Is the server.properties valid?")

    def help_show_world(self):
        console.help('This will give you the level names for your server, as well as total sizes (human readable)')

    def do_show_ops(self, line):
        ops = self.svr.get_ops_file()
        if ops:
            console.info("Here are your ops\n {}".format(ops))

    def help_show_ops(self):
        console.help('This will show you the current ops of the server')

    def do_backup_world(self, line):
        self.svr.backup_worlds()

    def help_backup_world(self):
        console.help("This will backup your worlds in a zip format to the location configured in settings.json")
        backup_dir = self.svr.helpers.get_setting('backups', 'backup_dir')
        console.help("Currently your backup directory is configured to be {}".format(backup_dir))

    def do_update_jar(self, line):

        if self.svr.check_running():
            console.warning("Server is currently running - Please issue a stop command first")
            return

        # find out which url we are using - look for the jar type
        jar_type = helpers.get_setting('updates', 'jar_type')

        # build which url path we are using based on the jar type
        url_path = helpers.get_setting('updates', "{}_url".format(jar_type))

        # where are we storing the current jar?
        backup_folder = helpers.get_setting('updates', 'update_backup_folder')

        # server / jar paths
        server_path = helpers.get_setting('server', 'path')
        server_jar = helpers.get_setting('server', 'jar')

        # make sure we have a directory to backup files to
        helpers.ensure_dir_exists(os.path.join(self.svr.server_path, backup_folder))

        # let's get the current date as a usable format for the filename
        now = str(datetime.datetime.now())[:19]
        now = now.replace(":", "_")
        backup_filename = '{}_backup_{}'.format(now, server_jar)

        # backup path for the current server.jar
        backup_path = os.path.join(server_path, backup_folder, backup_filename)
        current_path = os.path.join(server_path, server_jar)

        # backup the server.jar
        logging.debug("backing up current jar from {} to {}".format(current_path,backup_path))

        shutil.copy(current_path, backup_path)

        logging.info("Backed up current jar to {}".format(backup_path))
        console.info("Backed up current jar to {}".format(backup_path))

        logging.info("Starting Download of new jar file")
        console.info("Starting Download of new jar file")

        # get the new file
        r = requests.get(url_path, allow_redirects=True)

        if r.content:
            # save the new jar
            open(os.path.join(server_path, server_jar), 'wb').write(r.content)
            logging.info("Saved New Jar File")
            console.info("Saved New Jar File")
        else:
            logging.critical("Unable to download new jar - Status Code: {}".format(r.status_code))
            logging.debug("Unable to download new jar - R.txt: {}".format(r.text))
            logging.debug("Unable to download new jar - R.content: {}".format(r.content))
            console.critical("Unable to download new jar - Status Code: {}".format(r.status_code))

        logging.debug("Asking to start the server after update...")
        resp = input("Do you wish to start the server now? y/n > ")

        logging.debug("User says {}".format(resp.lower()))

        if resp.lower() == "y":
            logging.debug("Starting Server after update")
            self.do_start(line)

    def help_update_jar(self):
        backup_folder = helpers.get_setting('updates', 'update_backup_folder')
        server_path = helpers.get_setting('server', 'path')
        console.help("Creates a backup of your current jar, and Updates your server jar")
        console.help("Your settings show that the backup would be put in this folder:")
        console.help("{}".format(os.path.join(server_path,backup_folder)))

    def do_search_log(self, line):
        server_path = helpers.get_setting('server', 'path')
        log_file = os.path.join(server_path, "logs", "latest.log")
        console.info("Searching for {} in logs".format(line))
        matches = self.svr.search_file(log_file, line)
        if matches:
            console.info("Here are the matches:".format(line))

            for match in matches:
                console.info("Line #{}\t{}".format(match[0], match[1]))
        else:
            console.info("Unable to find any matches for {}".format(line))

    def help_search_log(self):
        console.help("Searches the server latest.log for the phrase")
        console.help("Searches are NOT case sensitive, and search all or part of a phrase ")
        console.help("Example: search_log info will show the lines with the word 'info' in them, as well as the lines "
                     "numbers")

    def do_show_errors(self, line):

        errors = self.svr.search_for_errors()

        if errors:
            console.info("Here are the matches:".format(line))

            for match in errors:
                console.info("Line #{}\t{}".format(match[0], match[1]))

        else:
            console.info("Unable to find any errors or warnings")

    def help_show_errors(self):
        console.help("Searches the server latest.log for errors and warnings")
        console.help("show_status will also give you a count of errors/warnings found")

    def do_schedule_backup(self, line=None):

        freq = input("How often do you wish to backup? (Example: 3) > ")
        freq_type = input("Minutes or Hours? (m/h) > ")
        print(freq, freq_type)
        if freq_type.lower() == "m":
            console.info("Scheduling Backup every {} minutes")

        else:
            console.info("Scheduling Backup every {} hours")

    def help_schedule(self):
        console.help("Currently you can schedule the following commands:")
        console.help("Backup - x minutes/hours")
        console.help("Status - x minutes/hours")
        console.help("Restart - x minutes/hours")
        console.help('Example: schedule backup 3 hours')
        console.help('Example: schedule restart 48 hours')
        console.help('Example: schedule status 5 minutes')
