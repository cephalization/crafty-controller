Crafty Controller
=================

A simple CLI interface for management of Minecraft Common Tasks

Crafty Controller is a python program to help with Minecraft Server Administration. Particularly focused on Paper, Spigot, Bukkit implementations. Pure python, with very little external module requirements. Primarily a console application. Crafty offers a simple easy to use command line interface that are split into two subsystems. The Crafty subsystem is for management of the Crafty application, while the Server subsystem is for management tasks related to the Minecraft server it launches and controls for the user.

Features
--------
- A simple to use CLI for management of tasks / configs
- Very lightweight and low requirements
- Excellent resource management
- Tested on Python 3.6 and 3.7

Installation
------------
This assume you have python3, pip3, and virtualenv installed.
  
    $ virtualenv crafty_controller
    $ cd crafty_controller
    $ source bin/activate
    $ git clone https://gitlab.com/Ptarrant1/crafty-controller.git
    $ cd crafty-controller/
    $ pip3 install -r requirements.txt
    $ python run.py


Running
-----

    $ cd crafty_controller
    $ source bin/activate
    $ cd crafty_controller
    $ edit crafty/settings.json and setup your server path, jar, and other options
    $ python run.py

Documentation
-------------

Crafty documentation lives [here](https://gitlab.com/Ptarrant1/crafty-controller/wikis/home).

Meta
----

Phillip Tarrant - [@craftycontrol ](https://twitter.com/CraftyControl)

Distributed under the MIT license. See [LICENSE.txt](https://gitlab.com/Ptarrant1/crafty-controller/blob/master/LICENSE.txt) for more information.

https://gitlab.com/Ptarrant1/crafty-controller
