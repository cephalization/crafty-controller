# Changelog
All notable changes to this project will be documented in this file.

## [v1.1]

### Added
- Command Added: Server: Search logs function - Searched the logs for phrase entered
- Command Added: Server: Show Errors Function - Shows line number, errors, and count of errors 
- Command Added: Server: Update Jar - Updates your server jar, backing up original in the process
- Command Added: Server: Show Schedule

- Server Function Added: Orphaned Servers: Now when the server sub system is loaded, it will check for an orphaned server running in the background - It will then ask the user if they wish to kill this process.
- Server Function Added: Schedule System: The settings.json file now have a schedule system. Currently you can schedule backups and show_status on command prompt.
- Server Function Added HTTP status page: If enabled in settings.json, the server will spawn a webserver on port 8000 (also configurable) and show the server status stats.

- Requirement Added: the amazing schedule module from  https://pypi.org/project/schedule/
- Requirement Added: Tornado has been added to the requirements.txt 

### Changed
- Command Changed: Server: Exit - On exit, if the server it running, it will warn you. 
- Bug Fix: Added better error message when server jar couldn't be found.
- Logging: Now logging starts as INFO level, not debug. This is still overwritten from settings file, but this saves 2 debug lines that always got printed.  

### Removed
- Clear screen after ctrl+D to exit program
- Removed double entry for "Looking for orphaned servers"

## [1.0] - 2019-10-10
v1.0 was Released

## [1.0.beta.3] - 2019-10-09

### Added
- Command Added: Server: backup_world - backups current worlds (creates a folder with Y-m-d H-M and 3 zips)
- Command Added: Crafty: check version - checks for updates in the current branch - also shows the branch you are on
- Setting Added: Added the Crafty section - in that section is a "branch" setting -  dev / beta / master depending on the branch you want

### Changed
- Command Moved: Crafty: show_version - Moved this command from the main window to the Crafty sub menu

### Removed
- Removed random print command from Server Preloop Function


## [1.0.beta.2] - 2019-10-08

### Added
- Command Added: Crafty: get public_ip - shows external IP 
- Command Added: Crafty/Server: Help system for all commands

- Command Added: Server: Restart - Restarts the server (with/without warnings)
- Command Added: Server: Invoke Command - Sends a command to the server and shows results:
- Command Added: Server: Show Ops: Shows the ops on your server
- Command Added: Server: Show World: Shows the servers world(s) with size information - Also Disk Information (total/used/free space)

- Added requests to requirements.txt
- Added ability to force the server down should the server be unresponsive to stop commands (This is optional: You can leave it running)
- Note: Confirmed though testing - If Crafty Controller Crashes, the server will still stay up and be available in most cases!

- Creates a default settings file for crafty if the file is missing

### Changed
- Split logic of commands - Program now has 2 sub terminals - Craft / Server
- Versioning standard has now been set to be "v Major.Minor.Stage.#" - "Example: v 1.0.Beta.1"
- Complete re-write of the input system. Now using cmd module which gives us some much needed structure and shortcuts
- Help system started from CMD module
- Split the command structre into 2 subsystems (Crafty, Server) this allows you to jump into a subsystem and only see commands for that system
- server.py - Changed tail command to do the output / warning messages to remove repeating code

### Removed
- None


## [1.0.b1] - 2019-10-07
### Added
- Initial Launch of Beta 1


### Changed
- Initial Launch

### Removed
- None
