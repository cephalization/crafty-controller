import os
import sys
import json
import logging.config
import cmd

# our imports
from crafty.helpers import Console
from crafty.helpers import Helpers
from crafty._version import __version__
from crafty.craft_cmd import CraftyCmd
from crafty.server_cmd import ServerCmd

console = Console()
helpers = Helpers()


class MainPrompt(cmd.Cmd):
    """ The main command class - loads the other modules/prompts """
    global __version__

    # overrides the default intro
    intro = "/" * 75 + "\n"
    intro += '#\t\tWelcome to Crafty Controller - v.{}\t\t #'.format(__version__) + "\n"
    intro += "/" * 75 + "\n"
    intro += '#   Commands: \t\t\t\t\t\t\t\t #' + "\n"
    intro += '#   Crafty: \t Loads the Crafty Module - Used for Crafty Commands\t #' + "\n"
    intro += '#   Server: \t Loads the Server Module - Used for Server Commands\t #' + "\n"
    intro += '#' + "-" * 72 + "#" + "\n"
    intro += '#   Type \t ? or "Help" for help\t\t\t\t\t #' + "\n"
    intro += '#   Exit: \t Exit the program\t\t\t\t\t #' + "\n"
    intro += '/' * 75 + "\n"

    # overrides the default Prompt
    prompt = "Crafty Controller (v{}) > ".format(__version__)
    

    def do_EOF(self, line):
        """ Exits the main program via Ctrl -D Shortcut """
        sys.exit(0)

    def do_exit(self, line):
        """ Exits the main program """
        sys.exit(0)

    def help_exit(self):
        console.help("Stops the server if running, Exits the program")

    def do_crafty(self, line):
        crafty = CraftyCmd()
        crafty.cmdloop()

    def help_crafty(self):
        console.help("Loads the Crafty Subsystem - Used for Crafty Commands")

    def do_server(self, line):
        server = ServerCmd()
        server.cmdloop()

    def help_server(self):
        console.help("Loads the Server Module - Used for Server Commands")

    def do_clear(self, line):
        try:
            os.system('clear')
        except:
            os.system('cls')

    def help_clear(self):
        console.help("Clears the screen")




if __name__ == '__main__':
    """ Our Main Starter """

    # logging path
    log_file_path = os.path.join(os.getcwd(), 'logs')

    # make a dir if needed
    helpers.ensure_dir_exists(log_file_path)

    # our main log file, will be created if not there
    log_file = os.path.join(log_file_path, 'crafty.log')

    # our settings path
    settings_path = os.path.join(os.getcwd(), 'crafty', 'settings.json')

    # setup logging
    logging_path = os.path.join(os.getcwd(), 'crafty', 'logging.json')

    # wipe the log file if it's there
    with open(os.path.join(log_file), 'w'):
        pass

    # open our logging config file
    with open(logging_path, 'r') as logging_config_file:
        logging_config = json.load(logging_config_file)

    # tell the logger that we are using a dict for config
    logging.config.dictConfig(logging_config)

    logging.info("Program Started")

    # get settings file
    logging.debug('Checking for settings file: {}'.format(settings_path))

    # if the settings file doesn't exist, create a settings file
    if not helpers.check_file_exists(settings_path):
        logging.warning('Unable to find settings file: {} - Creating One'.format(settings_path))
        console.warning('Unable to find settings file: {} - Creating One'.format(settings_path))
        console.warning('Please edit the settings file to reflect your server setup')
        helpers.create_default_settings()

    # get the logging level from the config file
    logging_level_string = helpers.get_setting('logging', 'log_file_level')

    # if we have a string from the config file
    if logging_level_string:
        logging.info("Setting logging to {}".format(logging_level_string))

        # we have to create a handler to be able to set the logging level
        logger = logging.getLogger()
        logger.setLevel(logging_level_string.upper())

    # kick off the main program
    logging.debug("Starting Main Loop")

    # create our main prompt for the program
    main = MainPrompt()

    # start main cmd loop
    main.cmdloop()
